package com.retoOracle.RetoOracle.models;

import javax.persistence.*;

@Entity (name = "Persona")
public class UserModel {
    @Column (name = "USERID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userId;

    @Column(name = "LAST_NAME", nullable = false, length = 255)
    private String lastName;

    @Column(name = "USER_NAME", nullable = true, length = 255)
    private String userName;

    @Column(name = "AGE", nullable = true, length = 3)
    private Integer age;

    public UserModel() {
    }

    public UserModel(Integer userId, String lastName, String userName, Integer age) {
        this.userId = userId;
        this.lastName = lastName;
        this.userName = userName;
        this.age = age;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}


