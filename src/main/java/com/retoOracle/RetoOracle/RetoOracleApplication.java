package com.retoOracle.RetoOracle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetoOracleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetoOracleApplication.class, args);
	}

}
