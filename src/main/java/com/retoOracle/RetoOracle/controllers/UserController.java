package com.retoOracle.RetoOracle.controllers;

import com.retoOracle.RetoOracle.models.UserModel;
import com.retoOracle.RetoOracle.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/RetoOracle")
public class UserController {
    @Autowired

    UserService userService;

    @GetMapping("/users")
    public List<UserModel> getUser() {
        System.out.println("getUsers");

        return this.userService.findAll();
    }

    @GetMapping ("users/{id}")
    public ResponseEntity<Object> getUserById (@PathVariable Integer id) {

        System.out.println("Entramos en getUserById: " + id);
        Optional<UserModel> retrieveUser = this.userService.findById(id);

        if (retrieveUser.isPresent()){
            return new ResponseEntity<>(retrieveUser.get(), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser (@RequestBody UserModel user){

        System.out.println("Entra en addUser");
        System.out.println("IdUser: " + user.getUserId());
        System.out.println("LastName: " + user.getLastName());
        System.out.println("Username: " + user.getUserName());
        System.out.println("Age: " + user.getAge());

        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);
    }

    @PutMapping ("/users/{id}")
    public ResponseEntity<String> updateUser (@RequestBody UserModel user) {
        System.out.println("Entra en updateUser");
        System.out.println("IdUser: " + user.getUserId());
        System.out.println("LastName: " + user.getLastName());
        System.out.println("Username: " + user.getUserName());
        System.out.println("Age: " + user.getAge());

        boolean updateProduct = this.userService.update(user);
        if (updateProduct){
            return new ResponseEntity<>("Usuario Actualizado", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Usuario NO encontrado, no se actualiza", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping ("/users/{id}")
    public ResponseEntity<String> deleteProduct (@PathVariable Integer id) {
        System.out.println("Entra en deleteProduct");
        System.out.println("El Id del producto a borrar es: " + id);

        boolean deleteProduct = this.userService.deleteById(id);

        return new ResponseEntity<>(
                deleteProduct ? "Usuario borrado" : "Usuario no encontrado, no puede borrarse",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
