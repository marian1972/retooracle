package com.retoOracle.RetoOracle.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello {

    @RequestMapping("/")
    public ResponseEntity<String> index() {
        return new ResponseEntity<>("¡¡¡Hola, este es el reto de Oracle !!!", HttpStatus.OK);
    }

    @RequestMapping("/hello")
    //Al poner RequestParam quiere decir que eel parámetro va a venir de "fuera" y podemos poner un valor por defecto

    public String hello(@RequestParam(value="name", defaultValue = "Amigo") String name){
      //sustituye y lo formatea, diferente de poner sólo "+" que sólo concatena
        return String.format ("¡Hola %s este es el reto de Oracle!", name);
    }

    @RequestMapping("/RetoOracle")
    //Al poner RequestParam quiere decir que eel parámetro va a venir de "fuera" y podemos poner un valor por defecto

    public String retoOracle (@RequestParam(value="name", defaultValue = "Amigo") String name){
        //sustituye y lo formatea, diferente de poner sólo "+" que sólo concatena
        return String.format ("¡Hola %s este es el reto de Oracle!", name);
    }

}
