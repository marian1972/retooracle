package com.retoOracle.RetoOracle.services;

import com.retoOracle.RetoOracle.models.UserModel;
import com.retoOracle.RetoOracle.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll()  {
        System.out.println("servicio findAll");
        return this.userRepository.findAll();
    }

    public Optional<UserModel> findById (Integer id){
        System.out.println("servicio findById");
        return this.userRepository.findById(id);
    }

    public UserModel add (UserModel user){
        System.out.println("servicio Add");
        return this.userRepository.save(user);
    }

    public boolean update (UserModel user){
        System.out.println("Servicio UpdateById");
        boolean result = false;

        if (this.userRepository.findById(user.getUserId()).isPresent()) {
            System.out.println("Usuario encontrado a actualizar");
            this.userRepository.save(user);
            result= true;
        }
        else {
            System.out.println("Usuario NO encontrado a actualizar");
        }

        return result;
    }

    public boolean deleteById (Integer id) {
        System.out.println("Servicio DeleteById");
        boolean result = false;

        if (this.userRepository.findById(id).isPresent()) {
            System.out.println("Usuario encontrado a actualizar");
            this.userRepository.deleteById(id);
            result = true;
        } else {
            System.out.println("Usuario NO encontrado a actualizar");
        }

        return result;
    }

}

