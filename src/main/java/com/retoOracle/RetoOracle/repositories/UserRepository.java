package com.retoOracle.RetoOracle.repositories;

import com.retoOracle.RetoOracle.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Integer> {
}
